<?php
    include("../index.php");
    $schemaValidator= json_decode(file_get_contents('../schema/paymentmethod/index.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $acno = $request->acno;
        $query = "SELECT * FROM payment_method WHERE is_deleted = 'N'";
        $omsdbobjx->query($query);
        echo response("1","Success",$omsdbobjx->resultset());
    }
    else{
        echo response("0","Error!",$valid->error);
    }