<?php 
    include("../index.php");
    $schemaValidator= json_decode(file_get_contents('../schema/rider/forcelogout.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $rider_id = $request->rider_id;
        $acno = $request->acno;
        $query = "UPDATE riders SET login_status = 'N' WHERE id = '$rider_id' and acno = '$acno'";
        $omsdbobjx->query($query);
        if($omsdbobjx->execute($query)){
            $reg_id = riderregistrationid($rider_id);
            $res = array();
            $res['data']['status'] = "Force Logout";
            $res['data']['rider_id'] = $rider_id;
            $res['data']['acno'] = $acno;
            firebasenotification($reg_id,$res);
            echo response("1","Success",$request);
        }
        else{
            echo response("0","Error!",[]);
        }
    }
    else{
        echo response("0","Error!",$valid->error);
    }
    

