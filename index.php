<?php
    header('Content-Type: application/json');
    include("db/config.php");
    include("db/omsconfig.php");
    include("vendor/autoload.php");
    use JsonSchema\SchemaStorage;
    use JsonSchema\Validator;
    use JsonSchema\Constraints\Factory;
    $dbobjx = new Database;
    $omsdbobjx = new OmsDatabase;
    function response($status , $message , $payload = array()){
        $data = array(
            "status"=>$status,
            "message"=>$message,
            "payload"=>$payload 
        );
        return json_encode($data);
    }

    function requestvalidatearray($request,$schema){
        $jsonValidator = new Validator();
        $jsonToValidateObject = json_decode($request);
        $response = array();
        foreach($jsonToValidateObject as $value){
            $errors = array();
            $jsonValidator->validate($value, json_decode($schema));
            if ($jsonValidator->isValid()) {
                $status = 1;
                //echo "The supplied JSON validates against the schema.\n";
            } else {
                $status = 0;
                //echo "JSON does not validate. Violations:\n";
                foreach ($jsonValidator->getErrors() as $error) {
                    $errors[] = array(
                        "property"=> $error['property'],
                        "message"=>$error['message']
                    );
                    //echo sprintf("[%s] %s\n", $error['property'], $error['message']);
                }
            }
            $response[] = array(
                "status"=>$status,
                "error"=>$errors
            );
        }
        return json_encode($response);
    }
    
    function requestvalidateobject($request,$schema){
        $errors = array();
        $jsonValidator = new Validator();
        $jsonValidator->validate($request,$schema);
        if ($jsonValidator->isValid()) {
            $status = 1;
            //echo "The supplied JSON validates against the schema.\n";
        } else {
            $status = 0;
            //echo "JSON does not validate. Violations:\n";
            foreach ($jsonValidator->getErrors() as $error) {
                $errors[] = array(
                    "property"=> $error['property'],
                    "message"=>$error['message']
                );
                //echo sprintf("[%s] %s\n", $error['property'], $error['message']);
            }
        }
        $response = array(
            "status" => $status,
            "error"=>$errors
        );
        return json_encode($response);
    }
    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    function generatingSalt(){
        $salt = password_hash('Whatawonderfulday', PASSWORD_DEFAULT);
        return $salt;
    }
    function encryptString($salt,$password)
    {
        $saltedpass = $password . $salt;
        $hashpassword = hash('sha256', $saltedpass);
        return $hashpassword;
    }
    function sendMessage($mask,$mobileno,$message){
        $url = 'http://119.160.92.2:7700/sendsms_xml.html';
        $sendData = "<?xml version='1.0' encoding='UTF-8'?><SMSRequest><Username>03028501227</Username><Password>SELL01</Password><From>".$mask."</From><To>".$mobileno."</To><Message>".$message."</Message><urdu>X</urdu><statuscode>X</statuscode></SMSRequest>";
        return curlFunction($url,$sendData);
    }

    function curlFunction($url,$data,$headers = "",$userpwd = "",$type = ""){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        if($headers != ""){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if($userpwd != ""){
            curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
        }
        if($type == "PUT"){
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        }
        else if($type != ""){
            curl_setopt($ch, CURLOPT_POST, 0);
        }
        else{
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close ($ch);
        if($err){
            return $err;
        }
        else{
            return $result;
        }
    }

    function firebasenotification($reg_id,$data){
        $fields = array(
            'to' => $reg_id,
            'data' => $data
        );
        $url     = 'https://fcm.googleapis.com/fcm/send';
        $headers = array(
            'Authorization: key=' . "AIzaSyBtlTMmyjyO4_SjEWOo0Y30iap7aeWJvds",
            'Content-Type: application/json'
        );
        return curlFunction($url,json_encode($fields),$headers);
    }

    function riderregistrationid ($rider_id){
        $omsdbobjx = new OmsDatabase;
        $query = "SELECT IFNULL(reg_id,'0') as reg_id FROM riders WHERE id = '".$rider_id."'";
        $omsdbobjx->query($query);
        $result = $omsdbobjx->single();
        if($omsdbobjx->rowCount() > 0){
            return $result->reg_id;
        }
        else{
            return "0";
        }   
    }

    function multipleriderregistrationid ($acno){
        $omsdbobjx = new OmsDatabase;
        $query = "SELECT IFNULL(reg_id,'0') as reg_id , id  FROM riders WHERE acno = '".$acno."'";
        $omsdbobjx->query($query);
        $result = $omsdbobjx->resultset();
        if($omsdbobjx->rowCount() > 0){
            return $result;
        }
        else{
            return "0";
        }   
    }

    function bookingGenerationBykea ($order_id , $acno , $courier_acno,$shipperName,$shipperAddress,$shipperContact,$shipperEmail,$consigneeName,$consigneeAddress,$consigneeContact,$consigneeEmail,$collectionRequired,$productDetail,$productValue,$origCity,$destinationCountry,$destinationCity,$serviceCode,$parcelType,$pieces,$weight,$fragile,$shipperRefrence,$insuranceRequire,$insuranceValue,$shipperComment,$courierMappingID,$consignee_city_id,$pickup_location_id){
        $url = "https://sandbox.bykea.dev/api/v2/batch";
        $response =couriercredientals($acno,9);
        $data = json_decode($response);
        if($data->status){
            $shipperContact = ninetwoNumberfotmat($shipperContact);
            $consigneeContact = ninetwoNumberfotmat($consigneeContact);
            if($insuranceRequire == "Y"){
                $insuranceRequire = true;
            }
            else{
                $insuranceRequire = false;
            }
            $request = "{\n    \"meta\": {\n        \"external_reference\": \"any\",\n        \"service_code\": 100\n    },\n    \"customer\": {\n        \"phone\": \"$data->courier_user\"\n    },\n    \"pickup\": {\n        \"name\": \"$shipperName\",\n        \"phone\": \"$shipperContact\",\n        \"lat\": 31.462202,\n        \"lng\": 74.269879,\n        \"address\": \"$shipperAddress\",\n        \"gps_address\": \"$shipperAddress\"\n    },\n    \"bookings\": [\n        {\n            \"meta\": {\n                \"service_code\": 22\n            },\n            \"dropoff\": {\n                \"name\": \"$consigneeName\",\n                \"phone\": \"$consigneeContact\",\n                \"lat\": 31.46510448,\n                \"lng\": 74.3130090,\n                \"address\": \"$consigneeAddress\",\n                \"gps_address\": \"$consigneeAddress\"\n            },\n            \"details\": {\n                \"voice_note\": \"url\",\n                \"cod_value\": \"$productValue\",\n                \"reference\": \"$shipperRefrence\",\n                \"insurance\": false\n            }\n        }\n    ]\n}";
            $header = array(
                "Content-Type: application/json",
                "x-api-customer-token: $data->courier_apikey",
                "Cookie: by-kea=s%3Ag0u97hihxnZLILSKQz_iweZKJAIGwUzH.7hR65JJRkpOL6y0pIpaiEgHe3X16kOz7PNF%2BstZNyj4"
            );
            $result = json_decode(curlFunction($url,$request,$header));
            if($result->code == "200"){
                foreach($result->data->bookings as $value){
                    $booking_id = (string)$value->booking_id;
                    $data = array(
                        "status" => "1",
                        "message" => $booking_id
                    );
                    courierupdateorder($order_id,8,9,$booking_id,$consignee_city_id,$pickup_location_id,$courierMappingID);
                }
            }
            else{
                $data = array(
                    "status" => "0",
                    "message" => $result->message
                );
            }
            return json_encode($data);
        }
        else{
            return $response;
        }
    }

    function CNGenerationBlueEx ($order_id , $acno , $courier_acno,$shipperName,$shipperAddress,$shipperContact,$shipperEmail,$consigneeName,$consigneeAddress,$consigneeContact,$consigneeEmail,$collectionRequired,$productDetail,$productValue,$origCity,$destinationCountry,$destinationCity,$serviceCode,$parcelType,$pieces,$weight,$fragile,$shipperRefrence,$insuranceRequire,$insuranceValue,$shipperComment,$courierMappingID,$consignee_city_id,$pickup_location_id){
        $url = "benefitx.blue-ex.com/api/post.php";
        $xml = "<?xml version='1.0' encoding='utf-8'?> 
                <BenefitDocument> 
                    <AccessRequest> 
                        <DocumentType>1</DocumentType> 
                        <TestTransaction></TestTransaction>
                        <ShipmentDetail> 
                            <ShipperName>".$shipperName."</ShipperName>
                            <ShipperAddress>".$shipperAddress."</ShipperAddress>
                            <ShipperContact>".$shipperContact."</ShipperContact>
                            <ShipperEmail>".$shipperEmail."</ShipperEmail>
                            <ConsigneeName>".$consigneeName."</ConsigneeName> 
                            <ConsigneeAddress>".$consigneeAddress."</ConsigneeAddress> 
                            <ConsigneeContact>".$consigneeContact."</ConsigneeContact> 
                            <ConsigneeEmail>".$consigneeEmail."</ConsigneeEmail> 
                            <CollectionRequired>".$collectionRequired."</CollectionRequired> 
                            <ProductDetail>".$productDetail."</ProductDetail> 
                            <ProductValue>".$productValue."</ProductValue> 
                            <OriginCity>".$origCity."</OriginCity> 
                            <DestinationCountry>".$destinationCountry."</DestinationCountry> 
                            <DestinationCity>".$destinationCity."</DestinationCity> 
                            <ServiceCode>".$serviceCode."</ServiceCode> 
                            <ParcelType>".$parcelType."</ParcelType> 
                            <Peices>".$pieces."</Peices> 
                            <Weight>".$weight."</Weight> 
                            <Fragile>".$fragile."</Fragile> 
                            <ShipperReference>".$shipperRefrence."</ShipperReference> 
                            <InsuranceRequire>".$insuranceRequire."</InsuranceRequire> 
                            <InsuranceValue>".$insuranceValue."</InsuranceValue> 
                            <ShipperComment>".$shipperComment."</ShipperComment> 
                        </ShipmentDetail> 
                    </AccessRequest> 
                </BenefitDocument>";
        $response =couriercredientals($acno,1);
        $data = json_decode($response);
        if($data->status){
            $userpwd = $data->courier_user.":".$data->courier_password;
            $result = simplexml_load_string(curlFunction($url,array('xml'=>$xml),"",$userpwd));
            if($result->status == "1"){
                $consigmentNo = (string)$result->message;
                $data = array(
                    "status" => "1",
                    "message" => $consigmentNo
                );
                courierupdateorder($order_id,8,1,$consigmentNo,$consignee_city_id,$pickup_location_id,$courierMappingID);
            }
            else{
                $data = array(
                    "status" => "0",
                    "message" => "Blue Ex Courier API Error!"
                );
            }
            return json_encode($data);
        }
        else{
            return $response;
        }
    }

    function CNGENERATIONTCS($order_id , $acno , $courier_acno,$shipperName,$shipperAddress,$shipperContact,$shipperEmail,$consigneeName,$consigneeAddress,$consigneeContact,$consigneeEmail,$collectionRequired,$productDetail,$productValue,$origCity,$destinationCountry,$destinationCity,$serviceCode,$parcelType,$pieces,$weight,$fragile,$shipperRefrence,$insuranceRequire,$insuranceValue,$shipperComment,$courierMappingID,$consignee_city_id,$pickup_location_id){
        $url = "https://apis.tcscourier.com/production/v1/cod/create-order";
        $response =couriercredientals($acno,2);
        $data = json_decode($response);
        if($data->status){
            $request = array(
                "userName"=>$data->courier_user,
                "password"=>$data->courier_password,
                "costCenterCode"=>"038687",
                "consigneeName"=>$consigneeName,
                "consigneeAddress"=> preg_replace("/\r|\n/", "", $consigneeAddress),
                "consigneeMobNo"=>$consigneeContact,
                "consigneeEmail"=>$consigneeEmail,
                "originCityName"=>'karachi',
                "destinationCityName"=>$destinationCity,
                "weight"=>1,
                "pieces"=>1,
                "codAmount"=>$productValue,
                "customerReferenceNo"=>$shipperRefrence,
                "services"=>"O",
                "productDetails"=>"khaadi",
                "fragile"=>"No",
                "remarks"=>"none",
                "insuranceValue"=>0
            );
            $header = array(
                "accept: application/json",
                "content-type: application/json",
                "x-ibm-client-id: $data->courier_apikey"
            );
            $result = json_decode(curlFunction($url,json_encode($request),$header));
            if($result->returnStatus->status == "SUCCESS"){
                $consigmentNo = $result->bookingReply->result;
                $consigmentNo = explode(":", $consigmentNo);
                $consigmentNo = trim($consigmentNo[1]);
                $data = array(
                    "status" => "1",
                    "message" => $consigmentNo
                );
                courierupdateorder($order_id,8,2,$consigmentNo,$consignee_city_id,$pickup_location_id,$courierMappingID);
            }
            else if($result->httpCode == 401){
                $data = array(
                    "status" => "0",
                    "message" => $result->moreInformation
                );
            }
            else{
                $data = array(
                    "status" => "0",
                    "message" => "TCS Courier API Error!"
                );
            }
            return json_encode($data);
        }

    }

    function CNGenerationLeopards($order_id , $acno , $courier_acno,$shipperName,$shipperAddress,$shipperContact,$shipperEmail,$consigneeName,$consigneeAddress,$consigneeContact,$consigneeEmail,$collectionRequired,$productDetail,$productValue,$origCity,$destinationCountry,$destinationCity,$serviceCode,$parcelType,$pieces,$weight,$fragile,$shipperRefrence,$insuranceRequire,$insuranceValue,$shipperComment,$courierMappingID,$consignee_city_id,$pickup_location_id){
        $url = "http://new.leopardscod.com/webservice/bookPacketTest/format/json/";
        $response =couriercredientals($acno,3);
        $data = json_decode($response);
        if($data->status){
            $data= array(
                'api_key'=> $data->courier_user,
                'api_password'=> $data->courier_password,
                'booked_packet_weight'=> $weight,
                'booked_packet_vol_weight_w'=> "",
                'booked_packet_vol_weight_h'=> "",
                'booked_packet_vol_weight_l'=> "",
                'booked_packet_no_piece'=> $pieces,
                'booked_packet_collect_amount'=> $productValue,
                'booked_packet_order_id'=> $shipperRefrence,
                'origin_city'=> $origCity,    
                'destination_city'=> $destinationCity,
                'shipment_name_eng'=> $shipperName,
                'shipment_email'=> $shipperEmail,
                'shipment_phone'=> $shipperContact,
                'shipment_address'=> $shipperAddress,
                'consignment_name_eng'=> $consigneeName,
                'consignment_email'=> $consigneeEmail,
                'consignment_phone'=> $consigneeContact,
                'consignment_phone_two'=> '',
                'consignment_phone_three'=> '',
                'consignment_address'=> $consigneeAddress,
                'special_instructions'=> '-',
                'shipment_type'=> $serviceCode
            );
            $result = json_decode(curlFunction($url,$data,"",""));
            if($result->status == "1"){
                $consigmentNo= $result->track_number;
                $data = array(
                    "status" => "1",
                    "message" => $consigmentNo
                );
                courierupdateorder($order_id,8,3,$consigmentNo,$consignee_city_id,$pickup_location_id,$courierMappingID);
            }
            else if($result->status == "0"){
                $data = array(
                    "status" => "0",
                    "message" => $result->error
                );
            }
            else{
                $data = array(
                    "status" => "0",
                    "message" => "Leopard Courier API Error!"
                );
            }
            return json_encode($data);
        }
        else{
            return $response;
        }
    }

    function CNGenerationMNP($order_id , $acno , $courier_acno,$shipperName,$shipperAddress,$shipperContact,$shipperEmail,$consigneeName,$consigneeAddress,$consigneeContact,$consigneeEmail,$collectionRequired,$productDetail,$productValue,$origCity,$destinationCountry,$destinationCity,$serviceCode,$parcelType,$pieces,$weight,$fragile,$shipperRefrence,$insuranceRequire,$insuranceValue,$shipperComment,$courierMappingID,$consignee_city_id,$pickup_location_id){
        $url = "http://mnpcourier.com/mycodapi/api/Booking/InsertBookingData";
        $response =couriercredientals($acno,4);
        $data = json_decode($response);
        if($data->status){
            if($fragile == 'Y'){
                $fragile = 'Yes';
            }
            else{
                $fragile = 'No';
            }
            $data = array(
                "username"=>$data->courier_user,
                "password"=>$data->courier_password,
                "consigneeName" => $consigneeName,
                "consigneeAddress" => $consigneeAddress,
                "consigneeMobNo" => $consigneeContact,
                "consigneeEmail" => $consigneeEmail,
                "destinationCityName" => $destinationCity,
                "pieces" => $pieces,
                "weight" => $weight,
                "codAmount" => $productValue,
                "custRefNo" => $shipperRefrence,
                "productDetails" => $productDetail,
                "fragile" => $fragile,
                "service" => 'O',
                "remarks" => '',
                "insuranceValue" => $insuranceValue
            );
            $header = array(
                "Content-Type: application/json"
            );
            $result = json_decode(curlFunction($url,json_encode($data),$header));
            if((string)$result[0]->isSuccess === "true"){
                $consigmentNo = (string)$result[0]->orderReferenceId;
                $data = array(
                    "status" => "1",
                    "message" => $consigmentNo
                );
                courierupdateorder($order_id,8,4,$consigmentNo,$consignee_city_id,$pickup_location_id,$courierMappingID);
            }
            else{
                $data = array(
                    "status" => "0",
                    "message" => (string)$result[0]->message
                );
            }
            return json_encode($data);   
        }
        else{
            return $response;
        }
    }

    function CNGenerationCallCourier($order_id , $acno , $courier_acno,$shipperName,$shipperAddress,$shipperContact,$shipperEmail,$consigneeName,$consigneeAddress,$consigneeContact,$consigneeEmail,$collectionRequired,$productDetail,$productValue,$origCity,$destinationCountry,$destinationCity,$serviceCode,$parcelType,$pieces,$weight,$fragile,$shipperRefrence,$insuranceRequire,$insuranceValue,$shipperComment,$courierMappingID,$consignee_city_id,$pickup_location_id){
        $response =couriercredientals($acno,5);
        $data = json_decode($response);
        if($data->status){
            if($productValue < 1){
                $ServiceTypeId= "1";
            }
            else{
                $ServiceTypeId= "7";
            }
            $url = "http://cod.callcourier.com.pk/api/CallCourier/SaveBooking?loginId=test-0001&ConsigneeName=".urlencode($consigneeName)."&ConsigneeRefNo=".urlencode($shipperRefrence)."&ConsigneeCellNo=".urlencode($consigneeContact)."&Address=".urlencode($consigneeAddress)."&Origin=".urlencode($origCity)."&DestCityId=".urlencode($destinationCity)."&ServiceTypeId=".urlencode($ServiceTypeId)."&Pcs=".urlencode($pieces)."&Weight=".urlencode($weight)."&Description=NA&SelOrigin=Domestic&CodAmount=".urlencode($productValue)."&SpecialHandling=false&MyBoxId=&Holiday=false&remarks=NA&ShipperName=".urlencode($shipperName)."&ShipperCellNo=".urlencode($shipperContact)."&ShipperArea=1&ShipperCity=1&ShipperAddress=".urlencode($shipperAddress)."&ShipperLandLineNo=".urlencode($shipperContact)."&ShipperEmail=".urlencode($shipperEmail);
            $header = array(
                "Postman-Token: 534bff68-441f-4022-af2d-3bdb53145fd5",
                "cache-control: no-cache"
            );
            $result = json_decode(curlFunction($url,"",$header,"","Get"));
            $response = explode(",",$result->Response);
            if($response[0] == "true"){
                $consigmentNo = (string)$result->CNNO;
                $data = array(
                    "status" => "1",
                    "message" => $consigmentNo
                );
                courierupdateorder($order_id,8,5,$consigmentNo,$consignee_city_id,$pickup_location_id,$courierMappingID);
            }
            else if($response[0] == "false"){
                $data = array(
                    "status" => "0",
                    "message" => trim($response[1])
                );
            }
            else{
                $data = array(
                    "status" => "0",
                    "message" => "call Courier API Error!"
                );
            }
            return json_encode($data);
        }
        else{
            return $response;
        }
    }

    function CNGenerationRider($order_id , $acno , $courier_acno,$shipperName,$shipperAddress,$shipperContact,$shipperEmail,$consigneeName,$consigneeAddress,$consigneeContact,$consigneeEmail,$collectionRequired,$productDetail,$productValue,$origCity,$destinationCountry,$destinationCity,$serviceCode,$parcelType,$pieces,$weight,$fragile,$shipperRefrence,$insuranceRequire,$insuranceValue,$shipperComment,$courierMappingID,$consignee_city_id,$pickup_location_id){
        $response =couriercredientals($acno,6);
        $data = json_decode($response);
        if($data->status){
            if($productValue < 1){
                $ServiceTypeId= "1";
            }
            else{
                $ServiceTypeId= "2";
            }
            $url = "http://sapi.withrider.com/rider/v2/SaveBooking?loginId=".urlencode($data->courier_user)."&ConsigneeName=".urlencode($consigneeName)."&ConsigneeRefNo=".urlencode($shipperRefrence)."&ConsigneeCellNo=".urlencode($consigneeContact)."&Address=".urlencode($consigneeAddress)."&OriginCityId=".urlencode($origCity)."&DestCityId=".urlencode($destinationCity)."&ServiceTypeId=".$ServiceTypeId."&DeliveryTypeId=2&Pcs=".urlencode($pieces)."&Weight=".urlencode($weight)."&Description=NA&CodAmount=".urlencode($productValue)."&remarks=NA&ShipperAddress=".urlencode($shipperAddress)."&apikey=".$data->courier_password;
            $result = json_decode(curlFunction($url,"","","","Get"));
            if($result->status == "ÖK"){
                $consigmentNo = (string)$result->CNUM;
                $data = array(
                    "status" => "1",
                    "message" => $consigmentNo
                );
                courierupdateorder($order_id,8,6,$consigmentNo,$consignee_city_id,$pickup_location_id,$courierMappingID);
            }
            else if($result->status == "Not ÖK"){
                $data = array(
                    "status" => "0",
                    "message" => $result->message
                );
            }
            else{
                $data = array(
                    "status" => "0",
                    "message" => "Rider Courier API Error!"
                );
            }
            return json_encode($data);
        }
        else{
            return $response;
        }
    }
    function CNGenerationTrax($order_id , $acno , $courier_acno,$shipperName,$shipperAddress,$shipperContact,$shipperEmail,$consigneeName,$consigneeAddress,$consigneeContact,$consigneeEmail,$collectionRequired,$productDetail,$productValue,$origCity,$destinationCountry,$destinationCity,$serviceCode,$parcelType,$pieces,$weight,$fragile,$shipperRefrence,$insuranceRequire,$insuranceValue,$shipperComment,$courierMappingID,$consignee_city_id,$pickup_location_id,$courier_code){
        $response =couriercredientals($acno,7);
        $data = json_decode($response);
        if($data->status){
            if($insuranceRequire == "Y"){
                $insuranceRequire = 1;
            }
            else{
                $insuranceRequire = 0;
            }
            $url = "https://sonic.pk/api/shipment/book";
            $request = "service_type_id=".urlencode("1")."&pickup_address_id=".urlencode($courier_code)."&information_display=".urlencode("1")."&consignee_city_id=".urlencode($destinationCity)."&consignee_name=".urlencode($consigneeName)."&consignee_address=".urlencode($consigneeAddress)."&consignee_phone_number_1=".urlencode(contactNumberfotmat($consigneeContact))."&consignee_phone_number_2=".urlencode(contactNumberfotmat($consigneeContact))."&consignee_email_address=".urlencode($consigneeEmail)."&order_id=".urlencode($shipperRefrence)."&item_product_type_id=".urlencode("1")."&item_description=NA&item_quantity=".urlencode($pieces)."&item_insurance=".urlencode($insuranceRequire)."&item_price=".urlencode($productValue)."&pickup_date=".urlencode('Y-m-d')."&special_instructions=NA&estimated_weight=".urlencode($weight)."&shipping_mode_id=1&same_day_timing_id=2&amount=".urlencode($productValue)."&payment_mode_id=1&charges_mode_id=3&delivery_type_id=1";
            $header = array(
                "Authorization: $data->courier_user"
            );
            $result = json_decode(curlFunction($url,$request,$header));
            if($result->status == "0"){
                $consigmentNo = (string)$result->tracking_number;
                $data = array(
                    "status" => "1",
                    "message" => $consigmentNo
                );
                courierupdateorder($order_id,8,7,$consigmentNo,$consignee_city_id,$pickup_location_id,$courierMappingID);
            }
            else if($result->status == "1") {
                $data = array(
                    "status" => "0",
                    "message" => $result->message
                );
            }
            else{
                $data = array(
                    "status" => "0",
                    "message" => "Trax Courier API Error!"
                );
            }
            return json_encode($data);
        }
        else{
            return $response;
        }
    }
    
    function CNGenerationOrio($order_id , $consignee_city_id,$pickup_location_id){
        $data = array(
            "status" => "1",
            "message" => $order_id
        );
        courierupdateorder($order_id,8,8,$order_id,$consignee_city_id,$pickup_location_id,$courierMappingID);
    
        return json_encode($data);
    }

    function traxaddpickupaddress($acno,$contactperson_name,$phone,$email,$address,$city_id){
        $response = couriermappingid($city_id , '7');
        $result = json_decode($response);
        if($result->status){
            $response =couriercredientals($acno,7);
            $data = json_decode($response);
            if($data->status){
                $url = "https://sonic.pk/api/pickup_address/add";
                $header = array(
                    "Authorization: $data->courier_user"
                );
                $data = array(
                    "person_of_contact" => $contactperson_name,
                    "phone_number" => $phone,
                    "email_address" => $email,
                    "address" => $address,
                    "city_id" => $result->courier_city_code
                );
                return curlFunction($url,$data,$header);
            }   
            else{
                return $response;    
            }
        }
        else{
            return $response;
        }
    }

    function couriermappingid($city_id , $courier_id){
        $omsdbobjx = new OmsDatabase;
        $query = "SELECT courier_city_code FROM courier_mappings WHERE city_id = '$city_id' AND courier_id = '$courier_id'";
        $omsdbobjx->query($query);
        $result = $omsdbobjx->single();
        if($omsdbobjx->rowCount() < 1){
            $data = array(
                "status"=> "0",
                "message"=>"This Courier City Is Not Map"
            );
        }
        else{
            $data = array(
                "status"=> "1",
                "message"=>"Success",
                "courier_city_code" => $result->courier_city_code 
            );
        }
        return json_encode($data);
    }
    
    function couriercredientals($acno,$courier_id){
        $omsdbobjx = new OmsDatabase;
        $query = "SELECT `courier_acno`, `courier_user` , `courier_password` , courier_apikey , `status` FROM customer_courier_details WHERE acno = '".$acno."' AND courier_id = '".$courier_id."'";
        $omsdbobjx->query($query);
        $result = $omsdbobjx->single();
        if($omsdbobjx->rowCount() < 1){
            $data = array(
                "status"=> "0",
                "message"=>"This Courier Detail Is Empty"
            );
        }
        else{
            if($result->status == 'inactive'){
                $data = array(
                    "status"=>"0",
                    "message"=>"This Courier Is Inactive"
                );  
            }
            else{
                $courier_acno =  $result->courier_acno;
                $courier_user =  $result->courier_user;
                $courier_password =  $result->courier_password;
                $courier_apikey =  $result->courier_apikey;
                $data = array(
                    "status"=>"1",
                    "acno"=>$courier_acno,
                    "courier_user"=>$courier_user,
                    "courier_password"=>$courier_password,
                    "courier_apikey"=>$courier_apikey
                );           
            }
        }
        return json_encode($data);
    }
    function blueExservicecode($acno,$courier_acno){
        $url = "http://benefitx.blue-ex.com/api/custinfo.php?acno=".$courier_acno;
        $response =couriercredientals($acno,1);
        $data = json_decode($response);
        if($data->status){
            $userpwd = $data->courier_user.":".$data->courier_password;
            $result = simplexml_load_string(curlFunction($url,"","",$userpwd));
            $data = array(
                "status" => "1",
                "message" => "Success",
                "service_code" => (string)$result->service_code
            );
        }
        else{
            $data = array(
                "status" => "0",
                "message" => "Courier API Error!"
            );
        }
        return json_encode($data);
    }

    function courierupdateorder($order_id , $status_id , $courier_id,$consigment_no,$consignee_city_id,$pickup_location_id,$courierMappingID){
        $omsdbobjx = new OmsDatabase;
        $query = "INSERT INTO order_status (order_id , status_id) VALUES ('".$order_id."','".$status_id."')";
        $omsdbobjx->query( $query);
        $omsdbobjx->execute($query);
        if($courier_id == "9" || $courier_id == "8"){
            $query = "UPDATE order_master SET order_last_status_id = '".$status_id."' , order_last_status_date = NOW() , courier_id = '".$courier_id."' , consigment_no = '".$consigment_no."' , destination_city_id = $consignee_city_id , origin_city_id = $pickup_location_id WHERE id = '".$order_id."'";   
        }
        else{
            $query = "UPDATE order_master SET order_last_status_id = '".$status_id."' , order_last_status_date = NOW() , courier_id = '".$courier_id."' , consigment_no = '".$consigment_no."' , courier_mapping_id = $courierMappingID , destination_city_id = $consignee_city_id , origin_city_id = $pickup_location_id  WHERE id = '".$order_id."'";
        }
        $omsdbobjx->query( $query);
        if($omsdbobjx->execute($query)){
            $data = array(
                "status" => 1,
                "message" => "Success"
            );
        }
        else{
            $data = array(
                "status" => 0,
                "message" => "Error"
            );
        }
        return json_encode($data);
    }

    function cancelblueexconsigment($acno , $courier_id , $order_id , $consigment_no){
        $url = "benefitx.blue-ex.com/api/post.php";
        $response =couriercredientals($acno,1);
        $data = json_decode($response);
        if($data->status){
            $userpwd = $data->courier_user.":".$data->courier_password;
            $xml = "<?xml version='1.0' encoding='utf-8'?> 
                <BenefitDocument> 
                    <AccessRequest> 
                        <DocumentType>12</DocumentType> 
                        <TestTransaction></TestTransaction>
                        <ShipmentNumbers> 
                            <Number>".$consigment_no."</Number>
                        </ShipmentNumbers> 
                    </AccessRequest> 
                </BenefitDocument>";
            $result = simplexml_load_string(curlFunction($url,array('xml'=>$xml),"",$userpwd));
            if($result->status){
                if($result->statusrow->status != "Not Void"){
                    cancelconsigmentorder($order_id);
                    $data = array(
                        "status"=>"1",
                        "message"=>"Void"
                    );           
                }
                else{
                    $data = array(
                        "status"=>"0",
                        "message"=>"Not Void"
                    );    
                }
            }
            else{
                $data = array(
                    "status"=>"0",
                    "message"=>"Not Void"
                );
            }
            return json_encode($data);
        }
        else{
            return json_encode($data);
        }
    }

    function canceltraxconsigment($acno , $courier_id , $order_id , $consigment_no){
        $response =couriercredientals($acno,7);
        $data = json_decode($response);
        if($data->status){
            $url = "https://sonic.pk/api/shipment/cancel";
            $header = array(
                "Authorization: $data->courier_user"
            );
            $data = array(
                "tracking_number" =>$consigment_no
            );
            $result = json_decode(curlFunction($url,$data,$header));
            if($result->status == "0"){
                cancelconsigmentorder($order_id);
                $data = array(
                    "status"=>"1",
                    "message"=>"Void"
                );
            }
            else{
                $data = array(
                    "status"=>"0",
                    "message"=>$result->message
                );
            }
            return json_encode($data);
        }
        else{
            return $response;
        }
    }

    function cancelmnpconsigment($acno , $courier_id , $order_id , $consigment_no){
        $response =couriercredientals($acno,4);
        $data = json_decode($response);
        if($data->status){
            $url = 'http://mnpcourier.com/mycodapi/api/Booking/VoidConsignment';
            $data = array (
                'username' => $data->courier_user,
                'password' => $data->courier_password,
                'consignmentNumberList' =>$consigment_no,
                'locationID' => '3020'
            );
            $params = "";
            foreach($data as $key=>$value){
                $params .= $key.'='.$value.'&';
            }
            $result = json_decode(curlFunction($url,$params));
            if($result[0]->isSuccess){
                $status = $result[0]->orderReferenceIdList[0];
                if($status->success){
                    cancelconsigmentorder($order_id);
                    $data = array(
                        "status" => "1",
                        "message" => "Void"
                    );
                }
                else{
                    $data = array(
                        "status" => "0",
                        "message" => (string)$status->message
                    );
                }
            }
            else{
                $data = array(
                    "status" => "0",
                    "message" => (string)$result[0]->message
                );
            }
            return json_encode($data);
        }
        else{
            echo $response;
        }
    }

    function canceltcsconsigment($acno , $courier_id , $order_id , $consigment_no){
        $response =couriercredientals($acno,2);
        $data = json_decode($response);
        if($data->status){
            $url = "https://apis.tcscourier.com/production/v1/cod/cancel-order";
            $request = array(
                "userName"=>$data->courier_user,
                "password"=>$data->courier_password,
                "consignmentNumber"=>$consigment_no
            );
            $header = array(
                "accept: application/json",
                "content-type: application/json",
                "x-ibm-client-id: $data->courier_apikey"
            );
            $result = json_decode(curlFunction($url,json_encode($request),$header,'','PUT'));
            if($result->returnStatus->code == "0200"){
                $data = array(
                    "status"=>"1",
                    "message"=>"Void"
                );
                cancelconsigmentorder($order_id);
            }
            else{
                $data = array(
                    "status"=>"0",
                    "message"=>$result->returnStatus->message
                );
            }
            return json_encode($data);
        }
        else{
            return $response;
        }
    }

    function cancelriderconsigment($acno , $courier_id , $order_id , $consigment_no){
        $response =couriercredientals($acno,6);
        $data = json_decode($response);
        if($data->status){
            $url = "http://sapi.withrider.com/rider/v1/CancelBooking?loginId=".urlencode($data->courier_user)."&CNNo=".$consigment_no."&apikey=".$data->courier_password;
            $result = json_decode(curlFunction($url,"","","","Get"));
            if($result->status == "ÖK"){
                $consigmentNo = (string)$result->CNUM;
                $data = array(
                    "status" => "1",
                    "message" => "Void"
                );
            }
            else{
                $data = array(
                    "status" => "1",
                    "message" => $result->message
                );
            }
            return json_encode($data);
        }
        else{
            return $response;
        }
    }

    function cancelleopardconsigment($acno , $courier_id , $order_id , $consigment_no){
        $response =couriercredientals($acno,3);
        $data = json_decode($response);
        if($data->status){
            $url = "http://new.leopardscod.com/webservice/cancelBookedPackets/format/json/";
            $data= array(
                'api_key'=> $data->courier_user,
                'api_password'=> $data->courier_password,
                'cn_numbers'=>$consigment_no
            );
            $result = json_decode(curlFunction($url,$data,"",""));
            if($result->status == "0"){
                $data = array(
                    "status"=>"0",
                    "message"=>$result->error->$consigment_no
                );
            }
            else{
                $data = array(
                    "status"=>"1",
                    "message"=>"Void"
                );
            }
            return json_encode($data);
        }
    }
    
    function cancelconsigmentorder($order_id){
        $omsdbobjx = new OmsDatabase;
        $query = "UPDATE order_master SET courier_id = NULL , courier_mapping_id = NULL , consigment_no = NULL , order_last_status_id = '7' , order_last_status_date = NOW() WHERE order_id = '$order_id'";
        $omsdbobjx->query( $query);
        $omsdbobjx->execute($query);
        $query = "INSERT INTO order_status() VALUES (order_id,status_id) VALUES ('".$order_id."','7')";
        $omsdbobjx->query( $query);
        $omsdbobjx->execute($query);
    }

    function contactNumberfotmat($consigneeContact){
        $consigneeContact  = str_replace("+920", "0", $consigneeContact);
        $consigneeContact  = str_replace("+92 0", "0", $consigneeContact);
        $consigneeContact  = str_replace("+ 92", "0", $consigneeContact);
        $consigneeContact  = str_replace("+92", "0", $consigneeContact);
        $consigneeContact  = str_replace("92", "0", $consigneeContact);
        $consigneeContact  = str_replace("+", "", $consigneeContact);
        $consigneeContact  = str_replace("+3", "03", $consigneeContact);
        $consigneeContact  = str_replace("O", "0", $consigneeContact);
        $consigneeContact  = trim($consigneeContact);
        return $consigneeContact;
    }
    function ninetwoNumberfotmat($consigneeContact){
        $consigneeContact  = str_replace("+920", "92", $consigneeContact);
        $consigneeContact  = str_replace("+92 0", "92", $consigneeContact);
        $consigneeContact  = str_replace("+ 92", "92", $consigneeContact);
        $consigneeContact  = str_replace("+92", "92", $consigneeContact);
        $consigneeContact  = str_replace("92", "92", $consigneeContact);
        $consigneeContact  = str_replace("+", "", $consigneeContact);
        $consigneeContact  = str_replace("+3", "92", $consigneeContact);
        $consigneeContact  = str_replace("O", "92", $consigneeContact);
        $consigneeContact  = trim($consigneeContact);
        return $consigneeContact;
    }