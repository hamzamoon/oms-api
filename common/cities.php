<?php
    include("../index.php");
    $schemaValidation= json_decode(file_get_contents('../schema/common/cities.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidation));
    if($valid->status){
        $country_id = $request->country_id;
        $where = "WHERE status = 'active' and country_id = '$country_id' ";
        $where .= !isset($request->province_id) ? '' :  " AND province_id='".$request->province_id."'";
        $query = "SELECT id ,  removeSpacialChar(`city_name`) city_name , country_id , province_id , created_at , updated_at FROM cities $where";
        $omsdbobjx->query($query);
        echo json_encode($omsdbobjx->resultset());
    }
    else{
        echo response("0","Error!",$valid->error);
    }