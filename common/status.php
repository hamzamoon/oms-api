<?php
    include("../index.php");
    $schemaValidation= json_decode(file_get_contents('../schema/common/status.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidation));
    if($valid->status){
        $status_type = $request->status_type;
        $query = "SELECT * FROM statues WHERE `status` = 'active' AND status_type = '$status_type'";
        $omsdbobjx->query($query);
        echo json_encode($omsdbobjx->resultset());
    }
    else{
        echo response("0","Error!",$valid->error);
    }