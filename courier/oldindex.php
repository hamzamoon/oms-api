<?php
    include("../index.php");
    $schemaValidator= json_decode(file_get_contents('../schema/courier/index.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $acno = $request->acno;
        $query = "SELECT couriers.*,(CASE WHEN customer_courier_details.id IS NULL THEN false ELSE true END) activate FROM couriers LEFT JOIN customer_courier_details ON customer_courier_details.courier_id = couriers.id AND customer_courier_details.acno = '".$acno."' WHERE couriers.status = 'active'";
        $omsdbobjx->query($query);
        echo json_encode($omsdbobjx->resultset());
    }
    else{
        echo response("0","Error!",$valid->error);
    }