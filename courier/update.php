<?php 
    include("../index.php");
    $update= json_decode(file_get_contents('../schema/courier/update.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$update));
    if($valid->status){
        $id = $request->id;
        $courier_acno = $request->courier_acno;
        $courier_id = $request->courier_id;
        $courier_user = $request->courier_user;
        $courier_password = $request->courier_password;
        $courier_apikey = $request->courier_apikey;
        $status = $request->status;
        $default = $request->default;
        $acno = $request->acno;
        if($default == "1"){
            $query =  "UPDATE customer_courier_details SET `default` = '0' WHERE acno = '".$acno."'"; 
            $omsdbobjx->query($query);
            $omsdbobjx->execute($query);
        }
        if($courier_id == '9'){
            $url = "https://sandbox-raptor.bykea.net/v2/authenticate/customer";
            $data = array(
                "username" => $courier_user,
                "password" => $courier_password
            );
            $header = array(
                "Content-Type: application/json"
            );
            $result = curlFunction($url,json_encode($data),$header);
            $response = json_decode($result);
            if($response->code != 200){
                echo response("0","Bykea token API Error",[]);
                return false;
            }
            $api_token = $response->data->token;
            $query = "UPDATE customer_courier_details SET `courier_apikey`= '".$api_token."' , `courier_acno` = '".$courier_acno."' , `courier_id` = '".$courier_id."' , `courier_user` = '".$courier_user."' , `courier_password` = '".$courier_password."' , `status` = '".$status."' , `default` = '".$default."' WHERE id = '$id'";                    
        }
        else{
            $query = "UPDATE customer_courier_details SET courier_apikey = '".$courier_apikey."' , `courier_acno` = '".$courier_acno."' , `courier_id` = '".$courier_id."' , `courier_user` = '".$courier_user."' , `courier_password` = '".$courier_password."' , `status` = '".$status."' , `default` = '".$default."' WHERE id = '$id'";
        }
        $omsdbobjx->query($query);
        if($omsdbobjx->execute($query)){
            echo response("1","Success",$request);
        }
        else{
            echo response("0","Error!",[]);
        }
    }
    else{
        echo response("0","Error!",$valid->error);
    }
    

