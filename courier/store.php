<?php 
    include("../index.php");
    $store= json_decode(file_get_contents('../schema/courier/store.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$store));
    if($valid->status){
        $acno = $request->acno;
        $user_id = $request->user_id;
        $courier_acno = $request->courier_acno;
        $courier_id = $request->courier_id;
        $courier_user = $request->courier_user;
        $courier_password = $request->courier_password;
        $courier_apikey = $request->courier_apikey;
        $query = "SELECT * FROM customer_courier_details WHERE acno = '$acno' AND courier_id = '$courier_id'";
        $omsdbobjx->query($query);
        $omsdbobjx->single();
        $result = $omsdbobjx->rowCount();
        if($result > 0){
            echo response("0","This courier Already Exist",[]);
            return false;
        }
        else{
            if($courier_id == '9'){
                $url = "https://sandbox-raptor.bykea.net/v2/authenticate/customer";
                $data = array(
                    "username" => $courier_user,
                    "password" => $courier_password
                );
                $header = array(
                    "Content-Type: application/json"
                );
                $result = curlFunction($url,json_encode($data),$header);
                $response = json_decode($result);
                if($response->code != 200){
                    echo response("0","Bykea token API Error",[]);
                    return false;
                }
                $api_token = $response->data->token;
                $query = "INSERT INTO customer_courier_details (`acno` , `user_id` , `courier_acno` , `courier_id` ,`courier_user` ,`courier_password`,`courier_apikey`) VALUES ('".$acno."','".$user_id."','".$courier_acno."','".$courier_id."','".$courier_user."','".$courier_password."','".$api_token."')";                    
            }
            else{
                $query = "INSERT INTO customer_courier_details (`acno` , `user_id` , `courier_acno` , `courier_id` ,`courier_user` ,`courier_password`,`courier_apikey`) VALUES ('".$acno."','".$user_id."','".$courier_acno."','".$courier_id."','".$courier_user."','".$courier_password."','".$courier_apikey."')";
            }
            $omsdbobjx->query($query);
            if($omsdbobjx->execute($query)){
                $id = $omsdbobjx->lastInsertId();
                $data[] = array(
                    "id"=>$id,
                    "acno"=>$acno,
                    "user_id"=>$user_id,
                    "courier_acno"=>$courier_acno,
                    "courier_id"=>$courier_id,
                    "courier_user"=>$courier_user,
                    "courier_password"=>$courier_password
                );
                echo response("1","Success",$data);
                return false;
            }
            else{
                echo response("0","Error",[]);
                return false;
            }
        }
    }
    else{
        echo response("0","Error!",$valid->error);
    }
    

