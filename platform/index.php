<?php
    include("../index.php");
    $schemaValidator= json_decode(file_get_contents('../schema/platform/index.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $acno = $request->acno;
        $query = "SELECT platforms.* , (CASE WHEN customer_platform_details.id IS NULL THEN false ELSE true END) activate FROM platforms LEFT JOIN customer_platform_details ON customer_platform_details.platform_id = platforms.id AND customer_platform_details.acno = '".$acno."' WHERE platforms.status = 'active'";
        $omsdbobjx->query($query);
        echo json_encode($omsdbobjx->resultset());
    }
    else{
        echo response("0","Error!",$valid->error);
    }