<?php
    include("../index.php");
    $schemaValidator= json_decode(file_get_contents('../schema/dashboard/graph.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $acno = $request->acno;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $query = "SELECT statues.`id` , statues.`name` ,  
        SUM((CASE WHEN order_master.id IS NOT NULL THEN 1 ELSE 0 END)) orders
        FROM statues 
        LEFT JOIN order_master ON order_master.`order_last_status_id` = statues.`id` AND
        DATE(order_master.created_at) BETWEEN '$start_date' AND '$end_date' AND acno = '$acno'
        WHERE status_type = 'Customer Service' AND statues.id NOT IN ('8')
        GROUP BY statues.`id` , statues.`name`
        ORDER BY id ASC";
        $omsdbobjx->query($query);
        $result = $omsdbobjx->resultset();
        echo response("1","Success",$result);
    }
    else{
        echo response("0","Error!",$valid->error);
    }
?>