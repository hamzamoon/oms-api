<?php
    include("../index.php");
    $schemaValidator= json_decode(file_get_contents('../schema/dashboard/graph.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $acno = $request->acno;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $query = "SELECT couriers.id , couriers.courier_name FROM customer_courier_details INNER JOIN couriers ON couriers.id = customer_courier_details.courier_id WHERE acno = '$acno'";
        $omsdbobjx->query($query);
        $result = $omsdbobjx->resultset();
        $data = array();
        foreach($result as $value){
            $courier_id=$value->id;
            $courier_name=$value->courier_name;
            $query = "SELECT statues.`id` , statues.`name` ,  
            SUM((CASE WHEN order_master.id IS NOT NULL THEN 1 ELSE 0 END)) orders
            FROM statues 
            LEFT JOIN order_master ON order_master.`order_last_status_id` = statues.`id` AND
            DATE(order_master.created_at) BETWEEN '$start_date' AND '$end_date' AND acno = '$acno'
            AND courier_id = '$courier_id'
            WHERE statues.id IN ('8','14','18','28','29')
            GROUP BY statues.`id` , statues.`name`
            ORDER BY id ASC";
            $omsdbobjx->query($query);
            $resultOrder = $omsdbobjx->resultset();
            $detail = array();
            foreach($resultOrder as $valueOrder){
                $detail[] = array(
                    'id'=>$valueOrder->id,
                    'name'=>$valueOrder->name,
                    'orders'=>$valueOrder->orders
                );
            }
            $data[] = array(
                'id' => $courier_id,
                'name' => $courier_name,
                'detail' => $detail
            );
        }
        echo response("1","Success",$data);
    }
    else{
        echo response("0","Error!",$valid->error);
    }
?>