<?php
    include("../index.php");
    $schemaValidator= json_decode(file_get_contents('../schema/dashboard/graph.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $acno = $request->acno;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $query = "SELECT DATE(created_at) AS orders_date , COUNT(*) AS orders , SUM(order_amount) as orders_amount, SUM(CASE WHEN t1.consignee_contact IS NOT NULL THEN 1 ELSE 0 END) AS customers FROM order_master LEFT JOIN 
        (SELECT consignee_contact FROM order_master GROUP BY consignee_contact HAVING COUNT(consignee_contact) <= 1) t1
        ON t1.consignee_contact = order_master.consignee_contact
        WHERE DATE(created_at) BETWEEN '$start_date' AND '$end_date' AND acno = '$acno'
        GROUP BY DATE(created_at)
        ORDER BY DATE(created_at) ASC ";
     
        $omsdbobjx->query($query);
        $result = $omsdbobjx->resultset();
        $totalOrders = 0;
        $totalCustomers = 0;
        $totalAmount = 0;
        $detail = array();
        foreach($result as $value){
            $detail[] = array(
                "date" => $value->orders_date,
                "orders" => $value->orders,
                "amount" => $value->orders_amount,
                "customers" =>  $value->customers
            );
            $totalOrders += $value->orders; 
            $totalCustomers += $value->customers; 
            $totalAmount += $value->orders_amount;
        }
        $data = array(
            "orders" => $totalOrders,
            "amount" => $totalAmount,
            "customers" => $totalCustomers,
            "detail" => $detail
        );
        echo response("1","Success",$data);
    }
    else{
        echo response("0","Error!",$valid->error);
    }
?>