<?php
    include("../index.php");
    $request = json_decode(file_get_contents('php://input'));
    $schemaValidator= json_decode(file_get_contents('../schema/shipment/create.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $data = array( );
        foreach($request->detail as $row){
            $order_id  = $row->order_id;
            $consignee_city_id  = $row->consignee_city_id;
            $pickup_location_id  = $row->pickup_location_id;
            $status = true;
            $query = "SELECT pickups.* , pickup_details.courier_code FROM pickups INNER JOIN pickup_details ON pickup_details.pickup_id = pickups.id WHERE pickups.id = '$pickup_location_id' AND courier_id = '$request->courier_id' AND acno = '$request->acno'";
            $omsdbobjx->query($query);
            $result = $omsdbobjx->single();
            if($omsdbobjx->rowCount() < 1){
                $data[] = array(
                    "status" => "0",
                    "message" => "This Pickup is not found",
                    "order_id" => $order_id
                );
            }
            else{
                $origin_city_id = $result->origin_city_id;
                $shipper_name = $result->contactperson_name;
                $shipper_address = $result->address;
                $shipper_email = $result->email;
                $shipper_contact = $result->phone;
                $courier_code = $result->courier_code;
                $query = "SELECT order_master.acno , customer_courier_details.courier_acno,shipper_name,shipper_address,shipper_contact,shipper_email,consignee_name,
                consignee_address,consignee_contact,consignee_email,order_amount,origin.`courier_city_code` origin_city_code,countries.country_code,destination.`courier_city_code` destination_city_code,
                piece,weight,'' AS shipper_comment , destination.id as courier_mapping_id , order_ref FROM order_master 
                left join customer_courier_details on customer_courier_details.`acno` = order_master.`acno` 
                LEFT JOIN courier_mappings destination ON destination.`country_id` = order_master.`destination_country_id`  
                AND destination.`city_id` = $consignee_city_id AND destination.courier_id = customer_courier_details.`courier_id`  
                LEFT JOIN courier_mappings origin ON origin.`country_id` = order_master.`origin_country_id` 
                AND origin.`city_id` = $origin_city_id AND origin.courier_id = customer_courier_details.`courier_id` 
                LEFT JOIN countries ON countries.`id` = order_master.`destination_country_id` 
                WHERE order_master.id IN ($order_id) AND order_master.acno = '$request->acno' AND 
                customer_courier_details.`courier_id` = $request->courier_id";
                $omsdbobjx->query($query);
                $result = $omsdbobjx->single();
                if($omsdbobjx->rowCount() > 0){
                    $shipper_refrence = $order_id."-".$result->order_ref;
                    if($request->courier_id != "9" && $request->courier_id != "8"){
                        if($result->origin_city_code == ""){
                            $status = false;
                            $data[] = array(
                                "status" => "0",
                                "message" => "This origin city is not map",
                                "order_id" => $order_id
                            );
                        }
                        if($result->destination_city_code == ""){
                            $status = false;
                            $data[] = array(
                                "status" => "0",
                                "message" => "This destination city is not map",
                                "order_id" => $order_id
                            );
                        }   
                    }
                    if($request->courier_id == ""){
                        $status = false;
                        $data[] = array(
                            "order_id"=>$order_id,
                            "status"=>0,
                            "message"=>"Not Found"
                        );
                    }
                    if($status){
                        if($request->courier_id == 1){
                            $response = json_decode(blueExservicecode($result->acno,$result->courier_acno));
                            if($response->status){
                                $service_code = $response->service_code;
                                $response = CNGenerationBlueEx ($order_id ,$result->acno,$result->courier_acno , $shipper_name,$shipper_address,$shipper_contact,$shipper_email,$result->consignee_name,$result->consignee_address,$result->consignee_contact,$result->consignee_email,'Y','',$result->order_amount,$result->origin_city_code,$result->country_code,$result->destination_city_code,$service_code,$request->parcel_type,$result->piece,$result->weight,$request->fragile_require,$shipper_refrence,$request->insurance_require,$request->insurance_value,'',$result->courier_mapping_id,$consignee_city_id,$pickup_location_id);
                                $responseData = json_decode($response);
                                if($responseData->status){
                                    $data[] = array(
                                        "order_id"=>$order_id,
                                        "status"=>$responseData->status,
                                        "message"=>"Success",
                                        "cnno" =>$responseData->message 
                                    );
                                }
                                else{
                                    $data[] = array(
                                        "order_id"=>$order_id,
                                        "status"=>$responseData->status,
                                        "message"=>$responseData->message
                                    );
                                }
                            }
                            else{
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>"0",
                                    "message"=>"Blue Ex Service Code API Error"
                                );           
                            }
                        }
                        else if($request->courier_id == 2){
                            $responseData = json_decode(CNGENERATIONTCS($order_id ,$result->acno,$result->courier_acno , $shipper_name,$shipper_address,$shipper_contact,$shipper_email,$result->consignee_name,$result->consignee_address,$result->consignee_contact,$result->consignee_email,'Y','',$result->order_amount,$result->origin_city_code,$result->country_code,$result->destination_city_code,'overnight',$request->parcel_type,$result->piece,$result->weight,$request->fragile_require,$shipper_refrence,$request->insurance_require,$request->insurance_value,'',$result->courier_mapping_id,$consignee_city_id,$pickup_location_id));
                            if($responseData->status){
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>"Success",
                                    "cnno" =>$responseData->message 
                                );
                            }
                            else{
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>$responseData->message 
                                );
                            }
                        }
                        else if($request->courier_id == 3){
                            $responseData = json_decode(CNGenerationLeopards($order_id ,$result->acno,$result->courier_acno , $shipper_name,$shipper_address,$shipper_contact,$shipper_email,$result->consignee_name,$result->consignee_address,$result->consignee_contact,$result->consignee_email,'Y','',$result->order_amount,$result->origin_city_code,$result->country_code,$result->destination_city_code,'overnight',$request->parcel_type,$result->piece,$result->weight,$request->fragile_require,$shipper_refrence,$request->insurance_require,$request->insurance_value,'',$result->courier_mapping_id,$consignee_city_id,$pickup_location_id));
                            if($responseData->status){
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>"Success",
                                    "cnno" =>$responseData->message 
                                );
                            }
                            else{
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>$responseData->message 
                                );
                            }
                        }
                        else if($request->courier_id == 4){
                            $responseData = json_decode(CNGenerationMNP($order_id ,$result->acno,$result->courier_acno , $shipper_name,$shipper_address,$shipper_contact,$shipper_email,$result->consignee_name,$result->consignee_address,$result->consignee_contact,$result->consignee_email,'Y','',$result->order_amount,$result->origin_city_code,$result->country_code,$result->destination_city_code,'overnight',$request->parcel_type,$result->piece,$result->weight,$request->fragile_require,$shipper_refrence,$request->insurance_require,$request->insurance_value,'',$result->courier_mapping_id,$consignee_city_id,$pickup_location_id));
                        if($responseData->status){
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>"Success",
                                    "cnno" =>$responseData->message 
                                );
                            }
                            else{
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>$responseData->message 
                                );
                            }
                        }
                        else if($request->courier_id == 5){
                            $responseData = json_decode(CNGenerationCallCourier($order_id ,$result->acno,$result->courier_acno , $shipper_name,$shipper_address,$shipper_contact,$shipper_email,$result->consignee_name,$result->consignee_address,$result->consignee_contact,$result->consignee_email,'Y','',$result->order_amount,$result->origin_city_code,$result->country_code,$result->destination_city_code,'overnight',$request->parcel_type,$result->piece,$result->weight,$request->fragile_require,$shipper_refrence,$request->insurance_require,$request->insurance_value,'',$result->courier_mapping_id,$consignee_city_id,$pickup_location_id));
                            if($responseData->status){
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>"Success",
                                    "cnno" =>$responseData->message 
                                );
                            }
                            else{
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>$responseData->message 
                                );
                            }
                        }
                        else if($request->courier_id == 6){
                            $responseData = json_decode(CNGenerationRider($order_id ,$result->acno,$result->courier_acno , $shipper_name,$shipper_address,$shipper_contact,$shipper_email,$result->consignee_name,$result->consignee_address,$result->consignee_contact,$result->consignee_email,'Y','',$result->order_amount,$result->origin_city_code,$result->country_code,$result->destination_city_code,'overnight',$request->parcel_type,$result->piece,$result->weight,$request->fragile_require,$shipper_refrence,$request->insurance_require,$request->insurance_value,'',$result->courier_mapping_id,$consignee_city_id,$pickup_location_id));
                            if($responseData->status){
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>"Success",
                                    "cnno" =>$responseData->message 
                                );
                            }
                            else{
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>$responseData->message 
                                );
                            }
                        }
                        else if($request->courier_id == 7){
                            $responseData = json_decode(CNGenerationTrax($order_id ,$result->acno,$result->courier_acno , $shipper_name,$shipper_address,$shipper_contact,$shipper_email,$result->consignee_name,$result->consignee_address,$result->consignee_contact,$result->consignee_email,'Y','',$result->order_amount,$result->origin_city_code,$result->country_code,$result->destination_city_code,'overnight',$request->parcel_type,$result->piece,$result->weight,$request->fragile_require,$shipper_refrence,$request->insurance_require,$request->insurance_value,'',$result->courier_mapping_id,$consignee_city_id,$pickup_location_id,$courier_code));
                            if($responseData->status){
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>"Success",
                                    "cnno" =>$responseData->message 
                                );
                            }
                            else{
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>$responseData->message 
                                );
                            }
                        }
                        else if($request->courier_id == 8){
                            $responseData = json_decode(CNGenerationOrio($order_id , $consignee_city_id,$pickup_location_id));
                            if($responseData->status){
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>"Success",
                                    "cnno" =>$responseData->message 
                                );
                            }
                            else{
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>$responseData->message 
                                );
                            }
                        }
                        else if($request->courier_id == 9){
                            $responseData = json_decode(bookingGenerationBykea($order_id ,$result->acno,$result->courier_acno , $shipper_name,$shipper_address,$shipper_contact,$shipper_email,$result->consignee_name,$result->consignee_address,$result->consignee_contact,$result->consignee_email,'Y','',$result->order_amount,$result->origin_city_code,$result->country_code,$result->destination_city_code,'overnight',$request->parcel_type,$result->piece,$result->weight,$request->fragile_require,$shipper_refrence,$request->insurance_require,$request->insurance_value,'',$result->courier_mapping_id,$consignee_city_id,$pickup_location_id));
                            if($responseData->status){
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>"Success",
                                    "cnno" =>$responseData->message 
                                );
                            }
                            else{
                                $data[] = array(
                                    "order_id"=>$order_id,
                                    "status"=>$responseData->status,
                                    "message"=>$responseData->message 
                                );
                            }
                        }
                    }
                }
                else{
                    $data[] = array(
                        "order_id"=>$order_id,
                        "status"=>0,
                        "message"=>"Not Found"
                    );
                }   
            }
        }
        echo response("1","Success",$data);
    }
    else{
        echo response("0","Error!",$valid->error);
    }
