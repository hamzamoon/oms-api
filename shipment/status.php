<?php
    include("../index.php");
    $request = json_decode(file_get_contents('php://input'));
    $schemaValidator= json_decode(file_get_contents('../schema/shipment/tracking.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $data = array();
        $orders = "";
        foreach($request as $value){
            $query = "SELECT order_status.created_at , statues.id , statues.name FROM order_status INNER JOIN statues ON statues.id = order_status.status_id WHERE order_id = $value->order_id GROUP BY order_status.status_id ORDER BY order_status.created_at ASC ";
            $omsdbobjx->query($query);
            $result = $omsdbobjx->resultset();
            $childData = array();
            foreach($result as $row){
                $childData[] = array(
                    "created_at" => $row->created_at,
                    "status_id" => $row->id,
                    "status_desc" => $row->name
                );
            }
            $data[] = array(
                "order_id" => $value->order_id,
                "detail" => $childData
            );
        }
        echo response("1","Success",$data);
    }
    else{
        echo response("0","Error!",$valid->error);
    }
?>