<?php
    include("../index.php");
    $request = json_decode(file_get_contents('php://input'));
    $schemaValidator= json_decode(file_get_contents('../schema/shipment/cancel.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $acno = $request->acno;
        $data = array( );
        foreach($request->detail as $row){
            $order_id  = $row->order_id;
            $query = "SELECT * FROM order_master where id = '$order_id' AND acno = '$acno'";
            $omsdbobjx->query($query);
            $result = $omsdbobjx->single();
            if($omsdbobjx->rowCount() > 0){
                $courier_id = $result->courier_id;
                $consigment_no = $result->consigment_no;
                if($consigment_no != ""){
                    if($courier_id == "1"){
                        $result = json_decode(cancelblueexconsigment($acno , $courier_id , $order_id , $consigment_no));
                    }
                    else if($courier_id == "2"){
                        $result = json_decode(canceltcsconsigment($acno , $courier_id , $order_id , $consigment_no));
                    }
                    else if($courier_id == "3"){
                        $result = json_decode(cancelleopardconsigment($acno , $courier_id , $order_id , $consigment_no));
                    }
                    else if($courier_id == "4"){
                        $result = json_decode(cancelmnpconsigment($acno , $courier_id , $order_id , $consigment_no));
                    }
                    else if($courier_id == "6"){
                        $result = json_decode(cancelriderconsigment($acno , $courier_id , $order_id , $consigment_no));
                    }
                    else if($courier_id == "7"){
                        $result = json_decode(canceltraxconsigment($acno , $courier_id , $order_id , $consigment_no));
                    }
                    else{
                        $result = array(
                            "status"=>"0",
                            "message"=>"Not Void"   
                        );
                        $result = json_encode($result);
                        $result = json_decode($result);
                    }
                    $data[] = array(
                        "status" => $result->status,
                        "message" => $result->message,
                        "order_id"=>$order_id
                    );
                }
                else{
                    $data[] = array(
                        "status"=>"0",
                        "message"=>"Consigment Not Found",
                        "order_id"=>$order_id,
                    );    
                }
            }
            else{
                $data[] = array(
                    "status"=>"0",
                    "order_id"=>$order_id,
                    "message"=>"Not Found"
                );   
            }
        }
        echo response("1","Success",$data);
    }
    else{
        echo response("0","Error!",$valid->error);
    }
