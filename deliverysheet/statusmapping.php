<?php 
    include("../index.php");
    $schemaValidation= json_decode(file_get_contents('../schema/deliverysheet/statusmapping.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidation));
    if($valid->status){
        $acno = $request->acno;
        $status_id = $request->status_id;
        $reattempt_allow = $request->reattempt_allow;
        $calling_allow = $request->calling_allow;
        $signature_allow = $request->signature_allow;
        $query = "SELECT COUNT(*) AS cc FROM statuses_customer_mapping WHERE acno = '$acno' AND status_id = '$status_id'";
        $omsdbobjx->query($query);
        $result = $omsdbobjx->single();
        $type;
        if($result->cc > 0 ){
            $query = "UPDATE statuses_customer_mapping SET reattempt_allow = '$reattempt_allow' , calling_allow = '$calling_allow' , signature_allow = '$signature_allow' WHERE acno = '$acno' AND status_id = '$status_id'";
            $type = "INSERT";
        }   
        else{
            $query = "INSERT INTO statuses_customer_mapping (acno , status_id , reattempt_allow , calling_allow , signature_allow) VALUES ('$acno' , '$status_id' ,'$reattempt_allow' , '$calling_allow' , '$signature_allow')";
            $type = "UPDATE";
        }
        $omsdbobjx->query($query);
        if($omsdbobjx->execute($query)){
            $query = "SELECT is_delivered , `name` FROM statues WHERE id = '$status_id'";
            $omsdbobjx->query($query);
            $result = $omsdbobjx->single();
            $is_delivered = $result->is_delivered;
            $status_name = $result->name;
            $result = multipleriderregistrationid($acno);
            foreach($result as $row){
                $reg_id = $row->reg_id;
                $rider_id = $row->id;
                $res = array();
                $res['data']['status'] = "StatusMapping";
                $res['data']['status_id'] = $status_id;
                $res['data']['status_name'] = $status_name;
                $res['data']['acno'] = $acno;
                $res['data']['rider_id'] = $rider_id;
                $res['data']['type'] = $type;
                $res['data']['is_delivered'] = $is_delivered;
                $res['data']['reattempt_allow'] = $reattempt_allow;
                $res['data']['calling_allow'] = $calling_allow;
                $res['data']['signature_allow'] = $signature_allow;
                firebasenotification($reg_id,$res);
            }
            echo response("1","Success",$request);
        }
        else{
            echo response("1","Error!",[]);
        }
    }
    else{
        echo response("0","Error!",$valid->error);
    }
    

