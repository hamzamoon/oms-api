<?php
    include("../index.php");
    $schemaValidator= json_decode(file_get_contents('../schema/deliverysheet/store.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $acno = $request->acno;
        $user_id = $request->user_id;
        $rider_id = $request->rider_id;
        $rout_id = $request->rout_id;
        $detail = $request->detail;
        $query = "INSERT INTO deliverysheet_master (`rider_id` , `rout_id`, `user_id` , `acno`) VALUES ('".$rider_id."','".$rout_id."','".$user_id."','".$acno."')";
        $omsdbobjx->query($query);
        if($omsdbobjx->execute($query)){
            $id = $omsdbobjx->lastInsertId();
            $orders = "";
            $querydetail = "INSERT INTO deliverysheet_detail (deliverysheet_id,order_id) VALUES ";
            $querystatus = "INSERT INTO order_status (order_id,status_id) VALUES ";
            foreach($detail as $value){
                $querydetail .= "($id,'$value->order_id'),";
                $querystatus .= "($value->order_id,26),";
                $orders .= $value->order_id.",";
            }
            $omsdbobjx->query( rtrim($querydetail, ','));
            $omsdbobjx->execute(rtrim($querydetail, ','));
            $omsdbobjx->query(rtrim($querystatus, ','));
            $omsdbobjx->execute(rtrim($querystatus, ','));
            $query = "UPDATE order_master SET order_last_status_id = '26' , order_last_status_date = NOW() WHERE id IN (". rtrim($orders, ',').")";
            $omsdbobjx->query($query);
            $omsdbobjx->execute($query);
            $reg_id = riderregistrationid($rider_id);
            $res = array();
            $res['data']['status'] = "DeliverySheet";
            $res['data']['rider_id'] = $rider_id;
            $res['data']['acno'] = $acno;
            $res['data']['deliverysheetno'] = $id;
            firebasenotification($reg_id,$res);
            $request->id = $id;
            echo response("1","Success",$request);
        }
        else{
            echo response("0","Error!",$request);    
        }
    }
    else{
        echo response("0","Error!",$valid->error);
    }
?>