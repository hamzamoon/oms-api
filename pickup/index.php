<?php
    try {
        include("../index.php");
        $registerSchema= json_decode(file_get_contents('../schema/pickup/index.json'));
        $request = json_decode(file_get_contents('php://input'));
        $valid = json_decode(requestvalidateobject($request,$registerSchema));
        if($valid->status){
            $data = array();
            $acno = $request->acno;
            $where = "";
            if(isset($request->pickup_id)){
                $where .= "AND id = '$request->pickup_id'"; 
            }
            $query = "SELECT * FROM pickups WHERE acno = '$acno' AND is_deleted = 'N' $where ORDER BY pickups.id DESC";
            $omsdbobjx->query($query);
            $result = $omsdbobjx->resultset();
            foreach($result as $value){
                $pickup_id = $value->id;
                $detail = array();
                $query = "SELECT pickup_details.* , `courier_name` FROM pickup_details LEFT JOIN couriers ON couriers.id = pickup_details.courier_id WHERE pickup_id = '$pickup_id'";
                $omsdbobjx->query($query);
                $detailresult = $omsdbobjx->resultset();
                foreach($detailresult as $detailvalue){
                    $detail[] = array(
                        "courier_id" => $detailvalue->courier_id,
                        "courier_name" => $detailvalue->courier_name
                    );
                }
                $data[] = array(
                    "id" => $pickup_id,
                    "acno" => $value->acno,
                    "origin_city_id" => $value->origin_city_id,
                    "pickuplocation_name" => $value->pickuplocation_name,
                    "contactperson_name" => $value->contactperson_name,
                    "address" => $value->address,
                    "email" => $value->email,
                    "phone" => $value->phone,
                    "created_at" => $value->created_at,
                    "updated_at" => $value->updated_at,
                    "default" => $value->default,
                    "status" => $value->status,
                    "user_id" => $value->user_id,
                    "is_deleted" => $value->is_deleted,
                    "detail" => $detail
                );
            }
            echo json_encode($data);   
        }    
        else{
            echo response("0","Error!",$valid->error);
        }
    }
    catch(Exception $error){
        echo response("0","Api Error!",$error);
    }