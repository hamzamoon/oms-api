<?php 
    include("../index.php");
    $store= json_decode(file_get_contents('../schema/pickup/store.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$store));
    if($valid->status){
        $id = $request->id;
        $acno = $request->acno;
        $user_id = $request->user_id;
        $origin_city_id = $request->origin_city_id;
        $pickuplocation_name = $request->pickuplocation_name;
        $contactperson_name = $request->contactperson_name;
        $address = $request->address;
        $phone = $request->phone;
        $email = $request->email;
        $default = $request->default;
        $status = $request->status;
        $detail = $request->detail;
        if($default == 'Y'){
            $query = "UPDATE pickups SET default = 'N' WHERE acno = '$acno'";
            $omsdbobjx->query($query);
            $omsdbobjx->execute($query);
        }
        $query = "UPDATE pickups SET `user_id` = '$user_id' , `origin_city_id` = '$origin_city_id' , `pickuplocation_name` = '$pickuplocation_name' ,`contactperson_name` = '$contactperson_name' ,`address` = '$address',`phone` = '$phone',`email` ='$email' , updated_at = NOW() , default = '$default' , `status` = '$status' WHERE `acno` = '$acno' AND id = '$id'";
        $omsdbobjx->query($query);
        if($omsdbobjx->execute($query)){
            $query = "UPDATE pickup_details SET pickup_id = NULL WHERE pickup_id = '$id'";
            $omsdbobjx->query($query);
            $omsdbobjx->execute($query);
            $query = "INSERT INTO pickup_details (pickup_id,courier_id,courier_code) VALUES ";
            $detaildata = array();
            foreach($detail as $row){
                if($row->courier_id == "7"){
                    $response = json_decode(traxaddpickupaddress($acno,$contactperson_name,$phone,$email,$address,$origin_city_id));
                    if($response->status == "0"){
                        $query .= "($id,'$row->courier_id','$response->id'),";
                        $detaildata[] = array(
                            "status" => "1",
                            "message" => "Success",
                            "courier_id" => $row->courier_id
                        );
                    }
                    else{
                        $detaildata[] = array(
                            "status" => "0",
                            "message" => $response->message,
                            "courier_id" => $row->courier_id
                        );
                    }
                }
                else{
                    $query .= "($id,'$row->courier_id',''),";
                    $detaildata[] = array(
                        "status" => "1",
                        "message" => "Success",
                        "courier_id" => $row->courier_id
                    );
                }                
            }
            $queryfinal = rtrim($query, ',');
            $omsdbobjx->query($queryfinal);
            $omsdbobjx->execute($queryfinal);
            $data[] = array(
                "id"=>$id,
                "acno"=>$acno,
                "user_id"=>$user_id,
                "origin_city_id"=>$origin_city_id,
                "pickuplocation_name"=>$pickuplocation_name,
                "contactperson_name"=>$contactperson_name,
                "address"=>$address,
                "phone"=>$phone,
                "detail" => $detaildata
            );
            echo response("1","Success",$data);
            return false;
        }
        else{
            echo response("0","Error",[]);
            return false;
        }
    }
    else{
        echo response("0","Error!",$valid->error);
    }
    

