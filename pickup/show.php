<?php
    include("../index.php");
    $schemaValidator= json_decode(file_get_contents('../schema/pickup/show.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $acno = $request->acno;
        $courier_id = $request->courier_id;
        $query="SELECT pickups.* FROM pickups 
                INNER JOIN pickup_details ON pickup_details.pickup_id = pickups.id
                WHERE acno = '".$acno."' AND pickups.is_deleted = 'N' 
                AND courier_id = '".$courier_id."' ORDER BY pickups.id ASC";
        $omsdbobjx->query($query);
        echo response("1","Success",$omsdbobjx->resultset());
    }
    else{
        echo response("0","Error!",$valid->error);
    }