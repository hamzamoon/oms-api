<?php 
    include("../index.php");
    $schemaValidator= json_decode(file_get_contents('../schema/rules/store.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$schemaValidator));
    if($valid->status){
        $acno = $request->acno;
        $status_id = $request->status_id;
        $sales_channel_id = ($request->sales_channel_id != "" || $request->sales_channel_id != null) ? "'".$request->sales_channel_id."'" : "NULL";
        $weight_type = ($request->weight_type != "" || $request->weight_type != null) ? "'".$request->weight_type."'" : "NULL";
        $weight_value = ($request->weight_value != "" || $request->weight_value != null) ? "'".$request->weight_value."'" : "NULL";
        $datetime_type = ($request->datetime_type != "" || $request->datetime_type != null) ? "'".$request->datetime_type."'" : "NULL";
        $datetime_value = ($request->datetime_value != "" || $request->datetime_value != null) ? "'".$request->datetime_value."'" : "NULL";
        $paymentmethod_id = ($request->paymentmethod_id != "" || $request->paymentmethod_id != null) ? "'".$request->paymentmethod_id."'" : "NULL";
        $customer_citylist_id = ($request->customer_citylist_id != "" || $request->customer_citylist_id != null) ? "'".$customer_citylist_id."'" : "NULL";
        $order_value_type = ($request->order_value_type != "" || $request->order_value_type != null) ? "'".$request->order_value_type."'" : "NULL";
        $order_value = ($request->order_value != "" || $request->order_value != null) ? "'".$request->order_value."'" : "NULL";
        $courier_id = $request->courier_id;
        if($status_id == null && $sales_channel_id == null && $weight_type == null && $weight_value == null && $datetime_type == null && $datetime_value == null && $paymentmethod_id == null && $customer_citylist_id == null && $order_value_type == null && $order_value == null){
            echo response("0","Please select atleast one rule",[]);    
            return false;
        }
        $query = "INSERT INTO customer_rules (acno , status_id , platform_id , weight_type , weight_value , datetime_type , datetime_value , paymentmethod_id , customer_citylist_id , order_value_type , order_value , courier_id) VALUES ('$acno' , '$status_id' , $sales_channel_id , $weight_type , $weight_value , $datetime_type , $datetime_value , $paymentmethod_id , $customer_citylist_id , $order_value_type , $order_value , '$courier_id')";
        $omsdbobjx->query($query);
        if($omsdbobjx->execute($query)){
            echo response("1","Success",[$request]);    
        }
    }
    else{
        echo response("0","Error!",$valid->error);
    }
    

