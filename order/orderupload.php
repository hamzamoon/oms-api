<?php
    include("../index.php");
    $registerSchema= json_decode(file_get_contents('../schema/order/create.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$registerSchema));
    if($valid->status){
        $payload = array();
        foreach($request as $value){
            $acno = $value->acno;
            $shipper_name = $value->shipper_name;
            $shipper_address = $value->shipper_address;
            $shipper_email = $value->shipper_email;
            $shipper_contact = $value->shipper_contact;
            $consignee_name = $value->consignee_name;
            $consignee_address = $value->consignee_address;
            $consignee_email = $value->consignee_email;
            $consignee_contact = $value->consignee_contact;
            $cnic_number = $value->cnic_number;
            $origin_country_id = $value->origin_country_id;
            $origin_province_id = $value->origin_province_id;
            $origin_city_id = $value->origin_city_id;
            $destination_country_id = $value->destination_country_id;
            $destination_province_id = $value->destination_province_id;
            $destination_city_id = $value->destination_city_id;
            $piece = $value->piece;
            $weight = $value->weight;
            $order_amount = $value->order_amount;
            $order_ref = $value->order_ref;
            $platform = $value->platform;
         
            $courier_id = $value->courier_id;
            $order_date = $value->order_date;
            $detail = $value->detail;
            try{
                $query = "INSERT INTO order_master(`acno`,`shipper_name`,`shipper_address`,`shipper_email`,`shipper_contact`,`consignee_name`,`consignee_address`,`consignee_email`,`consignee_contact`,`cnic_number`,`origin_country_id`,`origin_province_id`,`origin_city_id`,`destination_country_id`,`destination_province_id`,`destination_city_id`,`piece`,`weight`,`order_amount`,`order_ref`,`order_last_status_id`,`order_last_status_date`,`platform`,`created_at`) VALUES ('$acno','$shipper_name','$shipper_address','$shipper_email','$shipper_contact','$consignee_name','$consignee_address','$consignee_email','$consignee_contact','$cnic_number','$origin_country_id','$origin_province_id','$origin_city_id','$destination_country_id','$destination_province_id','$destination_city_id','$piece','$weight','$order_amount','$order_ref','1','$order_date','$platform','$order_date')";
               // echo   $query;
                $omsdbobjx->query($query);
                if($omsdbobjx->execute($query)){
                    $id = $omsdbobjx->lastInsertId();
                    $query = "INSERT INTO order_detail (order_id,product_code,product_name,quantity,amount) VALUES ";
                    foreach($detail as $row){
                        $query .= "($id,'$row->product_code','$row->product_name','$row->quantity','$row->amount'),";                
                    }
                    $queryfinal = rtrim($query, ',');
                    $omsdbobjx->query($queryfinal);
                    $omsdbobjx->execute($queryfinal);
                    $query = "INSERT INTO order_status (order_id , status_id) VALUES ($id,1)";
                    $omsdbobjx->query($query);
                    $omsdbobjx->execute($query);
                    // $query = "INSERT INTO order_status (order_id , status_id) VALUES ($id,8)";
                    // $omsdbobjx->query($query);
                    // $omsdbobjx->execute($query);
                    $payload[] = array(
                        "status"=> "1",
                        "message"=>"Success",
                        "order_id"=> $id,
                        "order_data"=>$value
                    );
    
                }
                else{
                    $payload[] = array(
                        "status"=> "0",
                        "message"=>"Error",
                        "order_data"=>$value
                    );
                }   
            }
            catch(Exception $error){
                $payload[] = array(
                    "status"=> "0",
                    "message"=>"Error",
                    "order_data"=>$value
                );
            }
        }
        echo response("1","Success",$payload);
    }
    else{
        echo response("0","Error!",$valid->error);
    }
?>