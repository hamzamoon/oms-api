<?php
    try{
        include("../index.php");
        $registerSchema= json_decode(file_get_contents('../schema/order/create.json'));
        $request = json_decode(file_get_contents('php://input'));
        $valid = json_decode(requestvalidateobject($request,$registerSchema));
        if($valid->status){
            $payload = array();
            foreach($request as $value){
                $acno = $value->acno;
                $shipper_name = $value->shipper_name;
                $shipper_address = $value->shipper_address;
                $shipper_email = $value->shipper_email;
                $shipper_contact = $value->shipper_contact;
                $consignee_name = $value->consignee_name;
                $consignee_address = $value->consignee_address;
                $consignee_email = $value->consignee_email;
                $consignee_contact = $value->consignee_contact;
                $cnic_number = $value->cnic_number;
                $origin_country_id = $value->origin_country_id;
                $origin_province_id = $value->origin_province_id;
                $origin_city_id = $value->origin_city_id;
                $destination_country_id = $value->destination_country_id;
                $destination_province_id = $value->destination_province_id;
                $destination_city_id = $value->destination_city_id;
                $piece = $value->piece;
                $weight = $value->weight;
                $order_amount = $value->order_amount;
                $order_ref = $value->order_ref;
                $platform_id = $value->platform_id;
                $payment_method_id = $value->payment_method_id;
                $remarks = $value->remarks;
                $shipping_charges = $value->shipping_charges;
                $billingperson_name = $value->billingperson_name;
                $billingperson_address = $value->billingperson_address;
                $billingperson_email = $value->billingperson_email;
                $billingperson_contact = $value->billingperson_contact;
                $detail = $value->detail;
                $query = "INSERT INTO order_master(`acno`,`shipper_name`,`shipper_address`,`shipper_email`,`shipper_contact`,`consignee_name`,`consignee_address`,`consignee_email`,`consignee_contact`,`cnic_number`,`origin_country_id`,`origin_province_id`,`origin_city_id`,`destination_country_id`,`destination_province_id`,`destination_city_id`,`piece`,`weight`,`order_amount`,`order_ref`,`order_last_status_id`,`order_last_status_date`,`platform_id`,`remarks`,`payment_method_id`,`shipping_charges`,`billingperson_name`,`billingperson_address`,`billingperson_contact`,`billingperson_email`) VALUES ('$acno','$shipper_name','$shipper_address','$shipper_email','$shipper_contact','$consignee_name','$consignee_address','$consignee_email','$consignee_contact','$cnic_number','$origin_country_id','$origin_province_id','$origin_city_id','$destination_country_id','$destination_province_id','$destination_city_id','$piece','$weight','$order_amount','$order_ref','1',NOW(),'$platform_id','$remarks','$payment_method_id','$shipping_charges','$billingperson_name','$billingperson_address','$billingperson_email','$billingperson_contact')";
                $omsdbobjx->query($query);
                if($omsdbobjx->execute($query)){
                    $id = $omsdbobjx->lastInsertId();
                    $query = "INSERT INTO order_detail (order_id,product_code,product_name,quantity,amount,image_url) VALUES ";
                    foreach($detail as $row){
                        $query .= "($id,'$row->product_code','$row->product_name','$row->quantity','$row->amount','$row->image_url'),";                
                    }
                    $queryfinal = rtrim($query, ',');
                    $omsdbobjx->query($queryfinal);
                    $omsdbobjx->execute($queryfinal);
                    $query = "INSERT INTO order_status (order_id , status_id) VALUES ($id,1)";
                    $omsdbobjx->query($query);
                    $omsdbobjx->execute($query);
                    $payload[] = array(
                        "status"=> "1",
                        "message"=>"Success",
                        "order_id"=> $id,
                        "order_data"=>$value
                    );
                }
                else{
                    $payload[] = array(
                        "status"=> "0",
                        "message"=>"Error",
                        "order_data"=>$value
                    );
                }
            }
            echo response("1","Success",$payload);
        }
        else{
            echo response("0","Error!",$valid->error);
        }   
    }catch(Exception $error){
        echo response("0","Api Error!",[]);
    }
?>