<?php
    try {
        include("../index.php");
        $registerSchema= json_decode(file_get_contents('../schema/order/order.json'));
        $request = json_decode(file_get_contents('php://input'));
        $valid = json_decode(requestvalidateobject($request,$registerSchema));
        if($valid->status){
            $acno = $request->acno;
            $startDate = $request->start_date;
            $endDate = $request->end_date;
            $startLimit = $request->start_limit - 1;
            $endLimit = $request->end_limit;
            if(isset($request->type)){
                $query = "SELECT order_master.* , statues.name as `status` , 'Yes' AS calling_confirmation , 'Yes' AS calling_cancellation FROM order_master 
                LEFT JOIN statues ON statues.id = order_master.order_last_status_id
                WHERE DATE(order_master.created_at) BETWEEN '$startDate' AND '$endDate' AND acno = '$acno'
                ORDER BY DATE(order_master.created_at) DESC"; 
            }
            else{
                $where = "";
                if(isset($request->platform_id)){
                    $where .= "AND platform_id IN ($request->platform_id)"; 
                }
                if(isset($request->status_id)){
                    $where .= "AND order_last_status_id IN ($request->status_id)"; 
                }
                if(isset($request->city_id)){
                    $where .= "AND destination_city_id IN ($request->city_id)"; 
                }
                
                $query = "SELECT order_master.* , statues.name as `status` , 'Yes' AS calling_confirmation , 'Yes' AS calling_cancellation FROM order_master LEFT JOIN statues ON statues.id = order_master.order_last_status_id 
                WHERE order_master.`created_at` BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59' AND acno = '".$acno."' AND status_type IN('Customer Service','System')  $where
                AND order_last_status_id NOT IN ('8') AND order_master.status NOT IN ('inactive') 
                ORDER BY id DESC LIMIT $startLimit, $endLimit";   
            }
            //echo  $query;
            $omsdbobjx->query($query);
            echo json_encode($omsdbobjx->resultset());   
        }    
        else{
            echo response("0","Error!",$valid->error);
        }
    }
    catch(Exception $error){
        echo response("0","Api Error!",$error);
    }