<?php
    include("../index.php");
    $registerSchema= json_decode(file_get_contents('../schema/order/edit.json'));
    $request = json_decode(file_get_contents('php://input'));
    $valid = json_decode(requestvalidateobject($request,$registerSchema));
    if($valid->status){
        $order_id = $request->order_id;
        $acno = $request->acno;
        $shipper_name = $request->shipper_name;
        $shipper_address = $request->shipper_address;
        $shipper_email = $request->shipper_email;
        $shipper_contact = $request->shipper_contact;
        $consignee_name = $request->consignee_name;
        $consignee_address = $request->consignee_address;
        $consignee_email = $request->consignee_email;
        $consignee_contact = $request->consignee_contact;
        $cnic_number = $request->cnic_number;
        $origin_country_id = $request->origin_country_id;
        $origin_province_id = $request->origin_province_id;
        $origin_city_id = $request->origin_city_id;
        $destination_country_id = $request->destination_country_id;
        $destination_province_id = $request->destination_province_id;
        $destination_city_id = $request->destination_city_id;
        $piece = $request->piece;
        $weight = $request->weight;
        $order_amount = $request->order_amount;
        $order_ref = $request->order_ref;
        $detail = $request->detail;
        $query = "SELECT COUNT(*) AS cc FROM order_master WHERE id = '$order_id' AND  courier_id IS NULL";
        $omsdbobjx->query($query);
        $omsdbobjx->single();
        $result = $omsdbobjx->rowCount();
        if($result > 0){
            $query = "UPDATE  order_master SET `acno` = '$acno',`shipper_name` = '$shipper_name',`shipper_address` = '$shipper_address',`shipper_email` = '$shipper_email',`shipper_contact` = '$shipper_contact',`consignee_name` = '$consignee_name',`consignee_address` = '$consignee_address',`consignee_email` = '$consignee_email',`consignee_contact` = '$consignee_contact',`cnic_number` = '$cnic_number',`origin_country_id` = '$origin_country_id',`origin_province_id` = '$origin_province_id',`origin_city_id` = '$origin_city_id',`destination_country_id` = '$destination_country_id',`destination_province_id` = '$destination_province_id',`destination_city_id` = '$destination_city_id',`piece` = '$piece',`weight` = '$weight',`order_amount` = '$order_amount',`order_ref` = '$order_ref' WHERE id = '$order_id'";
            $omsdbobjx->query($query);
            if($omsdbobjx->execute($query)){
                $query = "DELETE FROM order_detail WHERE order_id = '$order_id'";
                $omsdbobjx->query($query);
                $omsdbobjx->execute($query);
                $query = "INSERT INTO order_detail (order_id,product_code,product_name,quantity,amount) VALUES ";
                foreach($detail as $row){
                    $query .= "($order_id,'$row->product_code','$row->product_name','$row->quantity','$row->amount'),";                
                }
                $queryfinal = rtrim($query, ',');
                $omsdbobjx->query($queryfinal);
                $omsdbobjx->execute($queryfinal);
                echo response("1","Success",$request);
            }
            else{
                echo response("0","Error!",$request);
            }
        }
        else{
            echo response("0","Consigment Number Generated",$request);   
        }
    }
    else{
        echo response("0","Error!",$valid->error);
    }
?>